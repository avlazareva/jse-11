package ru.t1.lazareva.tm.api.controller;

public interface IProjectController {

    void clearProjects();

    void createProject();

    void showProjects();

    void removeProjectById();

    void removeProjectByIndex();

    void showProjectById();

    void showProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

}