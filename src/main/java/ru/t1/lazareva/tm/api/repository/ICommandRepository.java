package ru.t1.lazareva.tm.api.repository;

import ru.t1.lazareva.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}