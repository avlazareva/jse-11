package ru.t1.lazareva.tm.api.service;

import ru.t1.lazareva.tm.api.repository.ITaskRepository;
import ru.t1.lazareva.tm.model.Project;
import ru.t1.lazareva.tm.model.Task;

public interface ITaskService extends ITaskRepository {
    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

}